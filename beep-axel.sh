#!/bin/bash

SONG="Axel"

echo "This script uses the linux beep utility to play the song"
echo "Make sure it is installed, and it can play the sounds (e.g. on Ubuntu, the required module /"pcspkr/" is blacklisted, load it this way: sudo modprobe pcspkr && beep)"

echo "Playing $SONG!"

beep -f 659 -l 460 -n -f 784 -l 340 -n -f 659 -l 230 -n -f 659 -l 110 -n \
         -f 880 -l 230 -n -f 659 -l 230 -n -f 587 -l 230 -n \
         -f 659 -l 460 -n -f 988 -l 340 -n -f 659 -l 230 -n -f 659 -l 110 -n \
         -f 1047 -l 230 -n -f 988 -l 230 -n -f 784 -l 230 -n -f 659 -l 230 -n \
         -f 988 -l 230 -n -f 1318 -l 230 -n -f 659 -l 110 -n -f 587 -l 230 -n \
         -f 587 -l 110 -n -f 494 -l 230 -n -f 740 -l 230 -n -f 659 -l 460

echo "Done."

