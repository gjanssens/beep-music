#!/bin/bash

SONG="Canadian Snowtruck Warning"

echo "This script uses the linux beep utility to play the song"
echo "Make sure it is installed, and it can play the sounds (e.g. on Ubuntu, the required module /"pcspkr/" is blacklisted, load it this way: sudo modprobe pcspkr && beep)"

echo "Playing $SONG!"

for a in `seq 1 6`; do beep -f 1500 -l 200; beep -f 1550 -l 200; done

echo "Done."

