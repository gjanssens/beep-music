#!/bin/bash

echo "Playing random beep in folder:"
echo $PWD

BEEPAMOUNT=`ls -l beep*.sh | wc -l`
RANDOMBEEP=$[ 1 + $[ RANDOM % $BEEPAMOUNT ]]

BEEP=$(ls -l beep*.sh | sed -n ${RANDOMBEEP}p | awk '{ print $9 }')

echo "Playing $BEEP!"
./$BEEP
