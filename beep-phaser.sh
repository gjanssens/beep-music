#!/bin/bash

SONG="Phaser"

echo "This script uses the linux beep utility to play the song"
echo "Make sure it is installed, and it can play the sounds (e.g. on Ubuntu, the required module /"pcspkr/" is blacklisted, load it this way: sudo modprobe pcspkr && beep)"

echo "Playing $SONG!"

n=3000; while [ $n -gt 400 ]; do beep -f $n -l 5; n=$((n*97/100)); done

echo "Done."

