#!/bin/bash

SONG="Ring"

echo "This script uses the linux beep utility to play the song"
echo "Make sure it is installed, and it can play the sounds (e.g. on Ubuntu, the required module /"pcspkr/" is blacklisted, load it this way: sudo modprobe pcspkr && beep)"

echo "Playing $SONG!"

for n in 1 2 3 ; do
    for f in 1 2 1 2 1 2 1 2 1 2 ; do
        beep -f ${f}000 -l 20
    done
done

echo "Done."

