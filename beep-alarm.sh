#!/bin/bash

SONG="Alarm"

echo "This script uses the linux beep utility to play the song"
echo "Make sure it is installed, and it can play the sounds (e.g. on Ubuntu, the required module /"pcspkr/" is blacklisted, load it this way: sudo modprobe pcspkr && beep)"

echo "Playing $SONG!"

for n in 1 2 3 4 5 6 7 8 9 0; do
    for f in 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600; do
        beep -f $f -l 20
    done
done

echo "Done."

